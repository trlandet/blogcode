"""
Reset ubuntu/debian installed packages
=======================================

This script finds installed packages that are not dependencies of
packages you need, and are hence (possibly) leftover junk from a
previous distro version. This can happen if you continue upgrading
your Linux distro 

You must give the script a list of the packages you use, your
"keepers", i.e user programs like firefox, thunderbird, gimp,
krita, digikam, inkscape and more depending on what YOU actually
use. These packages and their dependencies will be kept.

You should also give a list of base packages, i.e the ones
installed by default on a clean system. You can get this list from
e.g. http://releases.ubuntu.com - pick the appropriate manifest
file. You can give the file name of one or more manifest files on
the command line.

Command line arguments:

 1) Package names of packages to keep
 2) File names ending in manifest (containing list of packages)
 3) The flag -v for verbose mode which will print all unnecessary
    packages instead of the default which just shows some
    statistics.

Tested on ubuntu 16.10. Do not trust this script completely, the
list of packages that are "safe" to remove may contain a few 
packages that you should not remove. You may e.g. have a special
network driver and removing this would cut your internet access
making it a bit annoying to fix it afterwards via sneakernet ...
"""
from __future__ import print_function, division
import sys
import apt
        

# Parse the command line arguments
ptk = sys.argv[1:]
packages_to_keep = []
manifests = []
for p in ptk:
    if p.endswith('.manifest'):
        manifests.append(p)
    elif not p.startswith('-'):
        packages_to_keep.append(p)
    elif p == '-v':
        verbose = True
    else:
        raise ValueError('Unknown command option %r' % p)
if not packages_to_keep:
    print(__doc__)
    print('\nERROR:')
    print("You must specify some packages to keep, ie. ubuntu-desktop etc")
    exit(1)


# Find installed packages, providers and dependicies
print('\n' + '-'*80, '\nReading package info')
installed_packages = set()
provided_packages = dict()
depended_packages = dict()
depending_packages = dict()
installed_versions = dict()
cache = apt.cache.Cache()
for package in cache:
    versions = [version for version in package.versions if version.is_installed]
    pname = package.name
    
    if len(versions) == 0:
        # This package is not installed
        continue
    elif len(versions) > 1:
        print('WARNING: Multiple package versions (%d) found for:' % len(versions), pname)
    
    installed_packages.add(pname)
    installed_versions.setdefault(pname, []).extend(versions)
    depended_packages.setdefault(pname, [])
    depending_packages.setdefault(pname, [])

    # Get any provided packages and dependencies
    for v in versions:
        for prov in v.provides:
            provided_packages.setdefault(prov, []).append(package.name)    
        for dep in v.dependencies:
            for base_dep in dep:
                dname = base_dep.name
                depended_packages[pname].append(dname)
                depending_packages.setdefault(dname, []).append(pname)
print('-'*80, '\nDone reading package info\n')


# Check that the packages we are asked to keep are installed
for kname in packages_to_keep:
    if kname in installed_packages:
        print('Package marked to keep:', kname)
    else:
        print('ERROR: cannot keep %s. It is not installed' % kname)
        exit()
print('\n' + '-'*80, '\n')


# Read manifest files (package name is first word on each line)
for manifest_file_name in manifests:
    for line in open(manifest_file_name):
        wds = line.split()
        mpname = wds[0]
        if mpname in installed_packages or mpname in provided_packages:
            packages_to_keep.append(mpname)
        else:
            print('Ignoring manifest package', mpname)


def get_providing_package(pname):
    """
    Return package objects, either the package with the
    provided name or an installed package that provides
    the requested meta/virtual/whaterver package with
    the fiven name
    """
    if pname in installed_packages:
        return cache[pname]
    for provider in provided_packages.get(pname, []):
        pp = cache[provider]
        if pp.is_installed:
            return pp
    return None

# Recursively get dependencies of the packages to keep so that we at the end
# will have a list of the keepers
all_keepers = set(packages_to_keep)
keeper_queue = list(packages_to_keep)
not_found = set()
while keeper_queue:
    kname = keeper_queue.pop()

    # The dependency may be a virtual package, get the real one
    pak = get_providing_package(kname)
    if pak is None:
        not_found.add(kname)
        continue
    
    # This package is required
    pname = pak.name
    all_keepers.add(pname)

    if not pname in installed_packages:
        print('WARNING: package not installed:', pname, pak.is_installed)
        print(repr(pak))
        exit()
    
    # Add the package's dependencies to the queue
    for dname in depended_packages[pname]:
        if not dname in all_keepers:
            keeper_queue.append(dname)


# There will be some strange packages that are dependent upon by
# installed packages, but for some reason are not installed
# Not much research has been done by the author to figure out
# why this happens as there does not seem to be very many of
# these strange missing packages
for nfname in sorted(not_found):
    if nfname in cache:
        print('NOT INSTALLED DEPENDENCY:', nfname)
    else:
        print('DID NOT FIND PACKAGE:', nfname)
        print('    Dependants:', ', '.join(depending_packages[nfname]))
print('\n' + '-'*80, '\n')


# Get the packages that are not dependent upon by our keepers
not_depended_upon = set()
possible_size_save = 0
for pname in installed_packages:
    if pname not in all_keepers:
        not_depended_upon.add(pname)
        for v in installed_versions[pname]:
            possible_size_save += v.installed_size


# Print package names of non-keepers, if the "-v" flag was given
if verbose:
    print('\n\nPackages that are not depended upon:\n' +'-'*80)
    for pname in sorted(not_depended_upon):
        print('   ', pname)
    print('-'*80, '\n')


# Print some statistics
print('There are %d installed packages' % len(installed_packages))
print('There are %d packages depended upon' % len(all_keepers))
print('There are %d packages that were not found, ' % len(not_found))
print()
print('There are %d packages NOT depended upon, ' % len(not_depended_upon))
print('  i.e %.2f%% of packages are not depended upon.' % 
      (100.0*len(not_depended_upon) / len(installed_packages)))
print('  They take up about %.3f GiB / %.3f GB of disk space' %
      (possible_size_save/2**30, possible_size_save/10**9))
