import sys
import os
import argparse
import shutil
from collections import OrderedDict
from PySide2 import QtGui, QtWidgets, QtCore
from PySide2.QtCore import Qt


EXTENSIONS = 'png jpg jpeg'.split()
RAW_FORMATS = 'raf nef raw xmp'.split()


class Picster:
    UNCATEGORIZED = 'uncategorized'

    def __init__(
        self,
        directory,
        keep_copy=True,
        dry_run=False,
        resort=False,
        verbose=False,
        pic_extensions=EXTENSIONS,
        raw_extensions=RAW_FORMATS,
    ):
        """
        Picster is a picture sorter. This class is the non-GUI part

        Initialize this class with a directory name and it will load all image
        file names into the .files attribute which is a dictionary mapping the
        image file name to a category. Set it to any category in the
        .categories list. The default is 'uncategorized'.

        After categorizing all images call .sort() to copy (or move, if keep_copy
        is set to False) images into directories with the category name.
        """
        self.directory = directory
        self.keep_copy = keep_copy
        self.dry_run = dry_run
        self.resort = resort
        self.verbose = verbose
        self.categories = [self.UNCATEGORIZED, 'meh', 'ok', 'good']
        self.pic_extensions = pic_extensions
        self.raw_extensions = raw_extensions
        self.refresh_images()

    def refresh_images(self):
        # Find images
        self.files = {}
        self.raw_files = {}
        for fn in os.listdir(self.directory):
            # Find the pics
            basename, ext = os.path.splitext(fn)
            if ext[1:].lower() not in self.pic_extensions:
                continue

            # Find possible pre-selected category
            self.files[fn] = self.UNCATEGORIZED
            for cat in self.categories[1:]:
                sorted_loc = os.path.join(self.directory, cat, fn)
                if os.path.isfile(sorted_loc):
                    self.files[fn] = cat

            # Find any non-pics
            for rawext in self.raw_extensions:
                for raw in (basename + '.' + rawext, basename + '.' + rawext.upper()):
                    raw_loc = os.path.join(self.directory, raw)
                    if os.path.isfile(raw_loc):
                        self.raw_files.setdefault(fn, []).append(raw)
                        for xmp in (raw + '.xmp', raw + '.XMP'):
                            xmp_loc = os.path.join(self.directory, xmp)
                            if os.path.isfile(xmp_loc):
                                self.raw_files.setdefault(fn, []).append(xmp)

        if self.resort:
            for cat in self.categories[1:]:
                cat_dir = os.path.join(self.directory, cat)
                if not os.path.isdir(cat_dir):
                    continue
                p = Picster(
                    cat_dir,
                    pic_extensions=self.pic_extensions,
                    raw_extensions=self.raw_extensions,
                    resort=False,
                )
                for fn in p.files:
                    if fn not in self.files:
                        fn2 = os.path.join(cat, fn)
                        self.files[fn2] = cat
                        self.raw_files[fn2] = [
                            os.path.join(cat, r) for r in p.raw_files.get(fn, ())
                        ]

        # Store in sorted order by basename
        fsort = sorted(self.files, key=lambda x: os.path.basename(x))
        self.files = OrderedDict((k, self.files[k]) for k in fsort)

    def sort(self):
        """
        Copy/move images into category directories. Uncategorized images
        are kept as is
        """
        # Pick the sorting function
        if self.keep_copy:
            verbose_func = lambda old, new: print('Copying %s => %s' % (old, new))  # NOQA
            sort_func = shutil.copy2
        else:
            verbose_func = lambda old, new: print('Moving %s => %s' % (old, new))  # NOQA
            sort_func = os.rename

        # Handle dry and verbose runs
        if self.dry_run:
            func = verbose_func
        elif self.verbose:
            func = lambda old, new: (verbose_func(old, new), sort_func(old, new))  # NOQA
        else:
            func = sort_func

        # Sort the pics
        for filename, category in self.files.items():
            self._sort_image(filename, func)

    def _sort_image(self, filename, sort_func):
        """
        Copy a given file to the category directory
        """
        # Ensure that the directory exists
        category = self.files[filename]
        if category is self.UNCATEGORIZED:
            dirname = self.directory
        else:
            dirname = os.path.join(self.directory, category)

        # Prepare to sort
        old_path = os.path.join(self.directory, filename)
        new_path = os.path.join(dirname, os.path.basename(filename))
        if os.path.abspath(old_path) == os.path.abspath(new_path):
            return

        # Sort the image
        if not os.path.isdir(dirname):
            os.mkdir(dirname)
        sort_func(old_path, new_path)

        # Sort any raw files
        for raw in self.raw_files.get(filename, ()):
            old_path = os.path.join(self.directory, raw)
            new_path = os.path.join(dirname, os.path.basename(raw))
            sort_func(old_path, new_path)

    def show_gui(self):
        """
        Show the Qt4 GUI for categorizing images
        """
        if not self.files:
            print('No images in this directory!')
            return 1

        app = QtWidgets.QApplication(sys.argv)
        gui = PicsterGUI(self)
        gui.show()
        return app.exec_()


class PicsterGUI(QtWidgets.QWidget):
    def __init__(self, picster, cache_size=10):
        """
        Simple GUI for the picture sorter program
        """
        super(PicsterGUI, self).__init__()
        self.picster = picster
        self.updateTitle()
        self.setWindowState(Qt.WindowMaximized)
        self.pixmaps = FifoCache(cache_size)
        self.layoutContent()
        self.index = 0
        self.updateImage()

    def updateTitle(self):
        N = len(self.picster.files)
        self.setWindowTitle('Picster - picture sorter - %d images' % N)

    def layoutContent(self):
        """
        Layout the widgets in the window
        """
        v = QtWidgets.QVBoxLayout()

        ###########################################
        # Category and sort buttons

        h = QtWidgets.QHBoxLayout()

        self.category_buttons = []
        for i, category in enumerate(self.picster.categories):
            b = QtWidgets.QPushButton('%d - %s' % (i, category))
            b.setCheckable(True)
            b.category_index = i
            b.clicked.connect(self.onClickCategory)
            b.setMinimumWidth(150)
            b.installEventFilter(self)
            h.addWidget(b)
            self.category_buttons.append(b)

        h.addStretch(1)
        b = QtWidgets.QPushButton("Sort", self)
        b.clicked.connect(self.onClickSort)
        b.installEventFilter(self)
        h.addWidget(b)

        v.addLayout(h)

        ###########################################
        # Image

        self.image = ImageViewlabel()
        self.image.setMinimumWidth(1)
        self.image.setMinimumHeight(1)

        v.addWidget(self.image, stretch=1)

        ###########################################
        # Previous and next buttons
        h = QtWidgets.QHBoxLayout()

        def swap_image(inc):
            self.index += inc
            self.updateImage()
            # HACK: make sure the the top level widget has focus so it gets key press events
            self.setFocus()

        prevb = QtWidgets.QPushButton('Previous image')
        prevb.clicked.connect(lambda: swap_image(-1))

        nextb = QtWidgets.QPushButton('Next image')
        nextb.clicked.connect(lambda: swap_image(+1))

        h.addStretch(1)
        h.addWidget(prevb)
        h.addSpacing(10)
        h.addWidget(nextb)
        h.addStretch(1)

        v.addLayout(h)

        ###########################################

        self.setLayout(v)
        self.setFocus()
        # self.setFocusPolicy(Qt.StrongFocus)

    def getImage(self, i):
        """
        Load an image from disk
        """
        if i not in self.pixmaps:
            image = list(self.picster.files)[i]
            fn = os.path.join(self.picster.directory, image)
            pic = QtGui.QPixmap(fn)
            self.pixmaps[i] = (image, pic)
        return self.pixmaps[i]

    def updateImage(self):
        """
        Update the current image and the selected category of the image
        """
        N = len(self.picster.files)
        if not N:
            return
        i = self.index % N
        img_name, pic = self.getImage(i)

        self.image_name = img_name
        self.image.pixmap = pic
        self.image.setImageSize(self.image.size())

        cat = self.picster.files[img_name]
        icat = self.picster.categories.index(cat)
        for i, b in enumerate(self.category_buttons):
            b.setChecked(i == icat)

    def onClickSort(self):
        """
        The user clicked the sort button
        """
        self.picster.sort()
        # HACK: make sure the the top level widget has focus so it gets key press events
        self.setFocus()

        if not self.picster.keep_copy:
            self.picster.refresh_images()
            self.index = 0
            self.pixmaps.clear()
            self.updateImage()
            self.updateTitle()

    def onClickCategory(self):
        """
        The user clicked a category button
        """
        b = self.sender()
        category = self.picster.categories[b.category_index]
        self.picster.files[self.image_name] = category
        self.updateImage()
        # HACK: make sure the the top level widget has focus so it gets key press events
        self.setFocus()

    def keyPressEvent(self, event):
        """
        The user pressed a key. Check for category (numbers) or image change (arrows)
        """
        key = event.key()

        # Try to convert to a number of a category
        try:
            category_id = int(event.text())
            category = self.picster.categories[category_id]
            self.picster.files[self.image_name] = category
            self.updateImage()
            return
        except (ValueError, IndexError):
            pass

        # Check if the user is changing the current image
        if key in (QtCore.Qt.Key_Left, QtCore.Qt.Key_Down, QtCore.Qt.Key_PageDown):
            self.index -= 1
            self.updateImage()
        elif key in (QtCore.Qt.Key_Right, QtCore.Qt.Key_Up, QtCore.Qt.Key_PageUp):
            self.index += 1
            self.updateImage()
        else:
            super(PicsterGUI, self).keyPressEvent(event)


class ImageViewlabel(QtWidgets.QWidget):
    def __init__(self, parent=None):
        """
        A displayer of images. Images are resized to fit in the
        available space. Set the current image to a pixmap in
        the .pixmap attribute. Then call .setImageSize with
        the available space as an argument.
        """
        super(ImageViewlabel, self).__init__(parent)
        self.pixmap = None

        self.img = QtWidgets.QLabel()

        h = QtWidgets.QHBoxLayout()
        h.addWidget(self.img, stretch=1, alignment=Qt.AlignCenter)

        self.setLayout(h)

    def setImageSize(self, size):
        """
        Set the available space for showing the image 
        """
        if self.pixmap is None:
            return
        scaled = self.pixmap.scaled(size, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.img.setPixmap(scaled)

    def resizeEvent(self, event):
        """
        The widget as been resized - resize the image as well so that it fits
        """
        self.setImageSize(event.size())


class FifoCache:
    def __init__(self, max_elems):
        """
        A dict-like fifo holding at most max_elems elements
        """
        self.max_elems = max_elems
        self._data = []

    def __setitem__(self, key, value):
        N = len(self._data)
        for i in range(N):
            if self._data[i][0] == key:
                self._data[i] = (key, value)
                return
        self._data.append((key, value))
        if N + 1 > self.max_elems:
            self._data = self._data[1:]

    def __getitem__(self, key):
        for k, v in self._data:
            if key == k:
                return v
        raise KeyError('No such key %r' % key)

    def __contains__(self, key):
        for k, v in self._data:
            if key == k:
                return True
        return False

    def clear(self):
        self._data = []


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'directory', help="The directory where the pictures to be sorted are located."
    )
    parser.add_argument(
        '--keep-copy',
        action="store_true",
        help="Do not move files into to subfolder, but copy them instead.",
    )
    parser.add_argument(
        '--dry-run',
        action="store_true",
        help="Do not actually sort, just show what would have been done.",
    )
    parser.add_argument(
        '--resort',
        action="store_true",
        help="Include pictures already sorted into subfolders in this sort.",
    )
    parser.add_argument(
        '--verbose',
        action="store_true",
        help="Show what is happening in the sort routine (always active in dry-runs).",
    )
    args = parser.parse_args()

    picster = Picster(
        args.directory,
        keep_copy=args.keep_copy,
        dry_run=args.dry_run,
        resort=args.resort,
        verbose=args.verbose,
    )
    sys.exit(picster.show_gui())


if __name__ == '__main__':
    main(sys.argv)
